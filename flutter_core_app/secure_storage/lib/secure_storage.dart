import 'dart:async';
import 'dart:io';

import 'package:flutter/services.dart';
import 'package:meta/meta.dart';

class SecureStorage {
  const SecureStorage();

  static const MethodChannel _channel =
      const MethodChannel('secure_storage');

  static Future<String> get platformVersion async {
    final String version = await _channel.invokeMethod('getPlatformVersion');
    return version;
  }

  Future<void> set({@required String key, @required String value, IOSOptions iOptions, AndroidOptions aOptions}) async =>
      _channel.invokeMethod('setFunc', <String, dynamic>{'key': key, 'value': value, 'options': _selectOptions(iOptions, aOptions)});

  Future<String> get({@required String key, IOSOptions iOptions, AndroidOptions aOptions}) async {
    final String value = await _channel.invokeMethod('getFunc', <String, dynamic>{'key': key, 'options': _selectOptions(iOptions, aOptions)});
    return value;
  }

  Future<void> remove({@required String key, IOSOptions iOptions, AndroidOptions aOptions}) =>
      _channel.invokeMethod('removeFunc', <String, dynamic>{'key': key, 'options': _selectOptions(iOptions, aOptions)});

  Future<Map<String, String>> getAll({IOSOptions iOptions, AndroidOptions aOptions}) async {
    final Map results = await _channel.invokeMethod('getAllFunc', <String, dynamic>{'options': _selectOptions(iOptions, aOptions)});
    return results.cast<String, String>();
  }

  Future<void> cleanUp({IOSOptions iOptions, AndroidOptions aOptions}) =>
      _channel.invokeMethod('cleanUpFunc', <String, dynamic>{'options': _selectOptions(iOptions, aOptions)});

  Map<String, String> _selectOptions(IOSOptions iOptions, AndroidOptions aOptions) {
    return Platform.isIOS ? iOptions?.params : aOptions?.params;
  }
}


abstract class Options {
  Map<String, String> get params => _toMap();

  Map<String, String> _toMap() {
    throw Exception('Not implementation');
  }
}

class IOSOptions extends Options {
  IOSOptions({String groupId, String service}) : _groupId = groupId, _keychainService = service;

  final String _groupId;
  final String _keychainService;

  @override
  Map<String, String> _toMap() {
    return <String, String>{'groupId': _groupId, 'keychainService': _keychainService};
  }
}

class AndroidOptions extends Options {
  AndroidOptions({String providerUri}) : _providerUri = providerUri;

  final String _providerUri;

  @override
  Map<String, String> _toMap() {
    return <String, String>{'uri': _providerUri};
  }
}
